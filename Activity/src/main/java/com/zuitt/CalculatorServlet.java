//Mathew A. Manalili CSIT343 - G2
package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	public void init() throws ServletException{
		System.out.println("*********************************************************");
		System.out.println(" CalculatorServlet has been initialized");
		System.out.println("*********************************************************");
	}
	public void destroy(){
		System.out.println("**********************************************************");
		System.out.println(" CalculatorServlet has been destroyed. ");
		System.out.println("**********************************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
	
		PrintWriter out= res.getWriter();
		out.println("<h1>You are now using the calculator app</h1>"+
		"<p>To use the app, input two nnumbers and ann operation."+"</p>"+
				"<p>Hit the submit button after filling in the details."+"</p>"+
		"<p>You will get the result shown in your browser!"+"</p>");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		int result = 0;
		//instead of having a lot of ors in an if statement you can use toLowerCase() method.
		//example: you input "ADD" to an input box and the equivalent word to access the operation add is "add" it will cause error.
		//The toLowerCase() method is used to convert the word that you input into lower case.
		// even if you type "add" to "ADD" or "AdD" or "ADd", it will just convert into "add"
		String operator= req.getParameter("operation").toLowerCase();
		if(operator.toLowerCase().equals("add")) {
			result = num1+num2;
		}
		else if(operator.toLowerCase().equals("subtract")) {
			result = num1-num2;
		}
		else if(operator.toLowerCase().equals("multiply")) {
			result = num1*num2;
		}
		else if(operator.toLowerCase().equals("divide")) {
			result = num1/num2;
		}
		PrintWriter out= res.getWriter();
		out.println("<p>The two numbers you provided are: "+num1+","+num2+"</p>"+
		"<p>The operation that you wanted is: "+operator+"</p>"+
				"<p>The result is: "+result);
	}
	
}
